clear
dt=0.001;
tmax=0.0625;
t=-tmax:dt:tmax;
x=cos(100*pi*t)+cos(200*pi*t)+sin(500*pi*t);
plot(t,x,'b', 'LineWidth', 2)