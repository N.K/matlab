clear
dt=0.001;
tmax=0.0625;
t=-tmax:dt:tmax;
x=cos(100*pi*t)+cos(200*pi*t)+sin(500*pi*t);
plot(t,x,'b', 'LineWidth', 5)
hold on

Ts=0.001;
n=[-tmax/Ts:tmax/Ts];

xs=cos(100*pi*n*Ts)+cos(200*pi*n*Ts)+sin(500*pi*n*Ts);

for k=1:length(t)
    xr (k) = xs * sinc ( (t(k)-n*Ts) /Ts  )';
end

plot (t,xr,'r', 'LineWidth', 2)
hold on

%Allagh ths periodou apo 0.001 se 0.0001
%Meiosh tis periodou => afksish ths suxnotitas deigmatolipsias
Ts=0.0001;
n=[-tmax/Ts:tmax/Ts];

xs=cos(100*pi*n*Ts)+cos(200*pi*n*Ts)+sin(500*pi*n*Ts);

for k=1:length(t)
    xr (k) = xs * sinc ( (t(k)-n*Ts) /Ts  )';
end

plot (t,xr,'--g', 'LineWidth', 2)


%Allagh ths suxnotitas apo 0.001 se 0.01
Ts=0.01;
n=[-tmax/Ts:tmax/Ts];

xs=cos(100*pi*n*Ts)+cos(200*pi*n*Ts)+sin(500*pi*n*Ts);

for k=1:length(t)
    xr (k) = xs * sinc ( (t(k)-n*Ts) /Ts  )';
end

plot (t,xr,'--m', 'LineWidth', 3)