%2d dct
function res=myDCT(signal)
    signal=double(signal);
    l=size(signal,1);
    res = zeros(l);
    for k=1:l     %1D DCT of each row of image
        res(k,:)=mdct(signal(k,:)');  
    end

    for k=1:l   %1D DCT of each column of image
        res(:,k)=mdct(res(:,k));
    end
end

function [ y ] = mdct( x )
  n=length(x);
  Co=(1/sqrt(2))*ones(1,n);
  a=[1:1:(n-1)]';%(n-1)x 1    
  b=2*[0:1:(n-1)]+1;%1x n
  A=repmat(a,1,n);%n-1 x n
  B=repmat(b,n-1,1);%n-1 x n
  R=(pi/(2*n))*(A.*B);
  C_R=cos(R);
  C=sqrt(2/n)*[Co;C_R];
  y=C*x;
end