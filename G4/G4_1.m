img = imread('linux.jpeg');

subplot(211)
imshow(img)
title('Original')

img = rgb2gray(img);

img_dct = myDCT(img);

img_dct(abs(img_dct) < 50) = 0;

g = idct2(img_dct)/255;

subplot(212)
imshow(g)
title('DCT of image')

imwrite(g, 'dct.jpeg')