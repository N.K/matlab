img = imread('linux.jpeg');

blurX = imgaussfilt(img, [1 8]);

subplot(121);
imshow(img)
title('Original Image')

subplot(122);
imshow(blurX);
title('Blurred Image');